class Person {
    constructor(name, age, occupation){
      this.name = name;
      this.age = age;
      this.occupation = occupation;
    }
  
    setName = () => (`My name is ${this.name}, I am ${this.age} years old, my occupation is a(an) ${this.occupation}`);
  }
  
  export default Person